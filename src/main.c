#include "tests.h"
#define TEST_QUANTITY 5

int main() {    
    int test_passed = 0;
    test_passed += allocate_and_free_test();
    separate();
    test_passed += free_one_block_test();
    separate();
    test_passed += free_two_blocks_test();
    separate();
    test_passed += grow_heap_test();
    separate();
    test_passed += grow_heap_somewhere_test();
    separate();
    if (test_passed == TEST_QUANTITY) printf("ALL TESTS ARE PASSED\n");
    else printf("SOME TESTS ARE FAILED. %d of %d TESTS ARE PASSED\n", test_passed, TEST_QUANTITY);

}
