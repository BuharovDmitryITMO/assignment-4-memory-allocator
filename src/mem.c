#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header *b, const char *fmt, ...);
void debug(const char *fmt, ...);

extern inline block_size size_from_capacity(block_capacity cap);
extern inline block_capacity capacity_from_size(block_size sz);

static bool block_is_big_enough(size_t query, struct block_header *block) { return block->capacity.bytes >= query; }
static size_t pages_count(size_t mem) { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t round_pages(size_t mem) { return getpagesize() * pages_count(mem); }

static void block_init(void *restrict addr, block_size block_sz, void *restrict next)
{
  *((struct block_header *)addr) = (struct block_header){
      .next = next,
      .capacity = capacity_from_size(block_sz),
      .is_free = true};
}

static size_t region_actual_size(size_t query) { return size_max(round_pages(query), REGION_MIN_SIZE); }

extern inline bool region_is_invalid(const struct region *r);

static void *map_pages(void const *addr, size_t length, int additional_flags)
{
  return mmap((void *)addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, -1, 0);
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region(void const *addr, size_t query)
{
  // Получаем размер области
  block_capacity capacity = {0};
  capacity.bytes = query;

  size_t size_of_block = size_from_capacity(capacity).bytes;
  size_t region_size = region_actual_size(size_of_block);

  // Выделяем память по адресу
  void *status = map_pages(addr, region_size, MAP_FIXED_NOREPLACE);

  if (status == MAP_FAILED)
  {
    status = map_pages(addr, region_size, 0);
  }
  if (status != MAP_FAILED)
  {
    block_size size = {0};
    size.bytes = region_size;

    // Добавляем новый блок
    block_init(status, size, NULL);

    struct region region = {0};
    region.addr = status;
    region.size = region_size;
    region.extends = status == addr;
    return region;
  }

  return REGION_INVALID;
}

static void *block_after(struct block_header const *block);

void *heap_init(size_t initial)
{
  const struct region region = alloc_region(HEAP_START, initial);
  if (region_is_invalid(&region))
    return NULL;

  return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable(struct block_header *restrict block, size_t query)
{
  return block->is_free && query + offsetof(struct block_header, contents) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big(struct block_header *block, size_t query)
{
  // Проверяем на возможность уменьшения
  if (!block_splittable(block, query)) return false;
  query = size_max(BLOCK_MIN_CAPACITY, query);

  // Создаем следующий блок с меньшим размером
  void *next_block = block->contents + query;
  block_size size = {0};
  size.bytes = block->capacity.bytes - query;
  block_init(next_block, size, block->next);

  // Обновляем блок
  block->next = next_block;
  block->capacity.bytes = query;

  return true;
}

/*  --- Слияние соседних свободных блоков --- */

static void *block_after(struct block_header const *block)
{
  return (void *)(block->contents + block->capacity.bytes);
}
static bool blocks_continuous(
    struct block_header const *fst,
    struct block_header const *snd)
{
  return (void *)snd == block_after(fst);
}

static bool mergeable(struct block_header const *restrict fst, struct block_header const *restrict snd)
{
  return fst->is_free && snd->is_free && blocks_continuous(fst, snd);
}

// Пытается объеденить последовательность блоков
static bool try_merge_with_next(struct block_header *block)
{
  if (!block || !block->next || !mergeable(block, block->next)) return false;

  // Создаём новый блок, объединяющий два блока
  block_size merged_block = {0};
  merged_block.bytes = size_from_capacity(block->capacity).bytes;
  merged_block.bytes += size_from_capacity(block->next->capacity).bytes;
  block_init(block, merged_block, block->next->next);

  return true;
}

/*  --- ... ecли размера кучи хватает --- */

struct block_search_result
{
  enum
  {
    BSR_FOUND_GOOD_BLOCK,
    BSR_REACHED_END_NOT_FOUND,
    BSR_CORRUPTED
  } type;
  struct block_header *block;
};

// Находит первый подходящий блок
static struct block_search_result find_good_or_last(struct block_header *restrict block, size_t size)
{
  // Блок-пустышка
  struct block_search_result result = {0};
  result.type = BSR_CORRUPTED;

  // Проверка на корректность блока
  if (!block) return result;

  while (block)
  {
    if (block->is_free)
    {
      // Объединяем все последующие блоки
      while (try_merge_with_next(block));

      // Если найденный блок имеет нужный размер
      if (block_is_big_enough(size, block))
      {
        result.type = BSR_FOUND_GOOD_BLOCK;
        result.block = block;
        return result;
      }
    }

    // Если куча закончилась
    if (!block->next)
    {
      result.type = BSR_REACHED_END_NOT_FOUND;
      result.block = block;
      return result;
    }

    // Итерирование
    block = block->next;
  }
  return result;
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing(size_t query, struct block_header *block)
{
  if (query <= BLOCK_MIN_CAPACITY ) query = BLOCK_MIN_CAPACITY;
  // Пытаемся найти подходящий блок
  struct block_search_result possible_block = find_good_or_last(block, query);

  // Проверяем подходит ли он
  if (possible_block.type != BSR_FOUND_GOOD_BLOCK) return possible_block;

  split_if_too_big(possible_block.block, query);
  possible_block.block->is_free = false;
  return possible_block;
}

// Увеличивает размер кучи
static struct block_header *grow_heap(struct block_header *restrict last, size_t query)
{
  if (!last) return NULL;

  // Пытаемся выделить память после последнего блока
  struct region region = alloc_region(block_after(last), query);
  if (region_is_invalid(&region)) return NULL;

  last->next = region.addr;
  // Возвращаем выделенный блок, если блоки не друг за другом
  if (!last->is_free || !region.extends) return last->next;

  //Если память выделилась за блоком, то объединяем блоки
  if (try_merge_with_next(last)) return last;

  return NULL;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header *memalloc(size_t query, struct block_header *heap_start)
{
  if (!heap_start) return NULL;
  query = size_max(BLOCK_MIN_CAPACITY, query);

  // Пытаемся найти блок без увелечения кучи
  struct block_search_result possible_block = try_memalloc_existing(query, heap_start);
  if (possible_block.type == BSR_REACHED_END_NOT_FOUND)
  {
    // Иначе увеличиваем кучу
    struct block_search_result new_block = possible_block;
    struct block_header *header = grow_heap(new_block.block, query);
    possible_block = try_memalloc_existing(query, header);
  }

  if (possible_block.type == BSR_CORRUPTED) return NULL;

  return possible_block.block;
}

void *_malloc(size_t query)
{
  struct block_header *const addr = memalloc(query, (struct block_header *)HEAP_START);
  if (addr)
    return addr->contents;
  else
    return NULL;
}

static struct block_header *get_block_header(void *contents)
{
  return (struct block_header *)(((uint8_t *)contents) - offsetof(struct block_header, contents));
}

void _free(void *mem)
{
  if (!mem)
    return;
  struct block_header *header = get_block_header(mem);
  header->is_free = true;
  while (try_merge_with_next(header));
}
