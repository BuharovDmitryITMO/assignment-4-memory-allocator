#ifndef TESTS
#define TESTS

#define _DEFAULT_SOURCE

#include "mem.h"
#include "mem_internals.h"
#include "util.h"
#include <sys/mman.h>

void separate();

bool allocate_and_free_test();

bool free_one_block_test();

bool free_two_blocks_test();

bool grow_heap_test();

bool grow_heap_somewhere_test();

#endif
