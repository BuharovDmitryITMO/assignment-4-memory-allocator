#include "tests.h"
static void free_heap(void *heap, size_t length)
{
    munmap(heap, length);
}

static struct block_header *get_block_header(void *contents)
{
    return (struct block_header *)(((uint8_t *)contents) - offsetof(struct block_header, contents));
}

void separate()
{
    printf("\n###################################################\n\n");
}

// Обычное выделение памяти и ее освобождение
bool allocate_and_free_test()
{
    printf("Test 1:\n\n");

    void *heap = heap_init(REGION_MIN_SIZE);
    debug_heap(stdout, heap);

    size_t capacity = 333;
    void *block = _malloc(capacity);
    debug_heap(stdout, heap);

    struct block_header *header = get_block_header(block);
    if (!header || !block || header->is_free || !(header->capacity.bytes >= capacity) || !(header->next->is_free))
    {
        printf("\nFAILED!\n");
        return false;
    }

    _free(block);
    debug_heap(stdout, heap);

    size_t heap_current_size = size_from_capacity(header->capacity).bytes;
    free_heap(heap, heap_current_size);

    printf("\nPASSED!\n");
    return true;
}

// Освобождение одного блока
bool free_one_block_test()
{
    printf("Test 2:\n\n");

    void *heap = heap_init(REGION_MIN_SIZE);
    debug_heap(stdout, heap);

    size_t capacity = 777;
    void *block1 = _malloc(capacity);
    void *block2 = _malloc(capacity);
    void *block3 = _malloc(capacity);
    debug_heap(stdout, heap);

    // Проверка правильности
    struct block_header *header1 = get_block_header(block1);
    struct block_header *header2 = get_block_header(block2);
    struct block_header *header3 = get_block_header(block3);
    if (!block1 || !block2 || !block3 || header1->is_free || header2->is_free || header3->is_free || !(header1->capacity.bytes >= capacity) || !(header2->capacity.bytes >= capacity) || !(header3->capacity.bytes >= capacity) || !header3->next || !header3->next->is_free || header3->next->next)
    {
        printf("\nFAILED!\n");
        return false;
    }

    _free(block2);
    debug_heap(stdout, heap);

    if (!block1 || !block3 || !header2->is_free)
    {
        printf("\nFAILED!\n");
        return false;
    }

    _free(block1);
    _free(block3);
    debug_heap(stdout, heap);

    if (!header1->is_free || !header1->next->is_free || header1->next->next)
    {
        printf("\nFAILED!\n");
        return false;
    }

    free_heap(heap, size_from_capacity(header1->capacity).bytes + size_from_capacity(header1->next->capacity).bytes);

    printf("\nPASSED!\n");
    return true;
}

// Тест освобождения двух блоков
bool free_two_blocks_test()
{
    printf("Test 3:\n\n");

    void *heap = heap_init(REGION_MIN_SIZE);
    debug_heap(stdout, heap);

    size_t capacity = 666;

    void *block1 = _malloc(capacity);
    void *block2 = _malloc(capacity);
    void *block3 = _malloc(capacity);
    debug_heap(stdout, heap);

    // Проверка правильности
    struct block_header *header1 = get_block_header(block1);
    struct block_header *header2 = get_block_header(block2);
    struct block_header *header3 = get_block_header(block3);
    if (!block1 || !block2 || !block3 || header1->is_free || header2->is_free || header3->is_free || !(header1->capacity.bytes >= capacity) || !(header2->capacity.bytes >= capacity) || !(header3->capacity.bytes >= capacity) || !header3->next || !header3->next->is_free || header3->next->next)
    {
        printf("\nFAILED!\n");
        return false;
    }

    _free(block2);
    _free(block1);
    debug_heap(stdout, heap);

    if (!block3 || !header1->is_free || !header3->next->is_free || header3->is_free || header3->next->next)
    {
        printf("\nFAILED!\n");
        return false;
    }

    _free(block3);
    debug_heap(stdout, heap);

    if (!header1->is_free || !header1->next->is_free || header1->next->next)
    {
        printf("\nFAILED!\n");
        return false;
    }

    size_t heap_current_size = size_from_capacity(header1->capacity).bytes + size_from_capacity(header1->next->capacity).bytes;
    free_heap(heap, heap_current_size);

    printf("\nPASSED!\n");
    return true;
}

// Добавление памяти, расширяя существующую
bool grow_heap_test()
{
    printf("Test 4:\n\n");

    void *heap = heap_init(REGION_MIN_SIZE);
    debug_heap(stdout, heap);

    struct block_header const *header = heap;
    size_t capacity = header->capacity.bytes;

    void *block = _malloc(capacity * 2);
    debug_heap(stdout, heap);

    // Проверка правильности
    if (!block || header->is_free || !header->next->is_free || header->capacity.bytes < capacity * 2 || header->next->next)
    {
        printf("\nFAILED!\n");
        return false;
    }

    _free(block);
    debug_heap(stdout, heap);

    if (!header->is_free || header->next)
    {
        printf("\nFAILED!\n");
        return false;
    }
    size_t heap_current_size = size_from_capacity(header->capacity).bytes + size_from_capacity(get_block_header(block)->capacity).bytes;
    free_heap(heap, heap_current_size);

    printf("\nPASSED!\n");
    return true;
}

// Добавление памяти, когда расширение невозможно
bool grow_heap_somewhere_test()
{
    printf("Test 5:\n\n");

    void *heap = heap_init(REGION_MIN_SIZE);
    debug_heap(stdout, heap);

    struct block_header const *header = heap;
    size_t capacity = header->capacity.bytes;
    size_t size = size_from_capacity(header->capacity).bytes;

    void *region = mmap((void *)(header->contents + header->capacity.bytes), REGION_MIN_SIZE, PROT_READ | PROT_WRITE,
                        MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED, -1, 0);

    if (!heap || region == MAP_FAILED)
    {
        printf("\nFAILED!\n");
        return false;
    }

    void *block = _malloc(capacity * 2);
    debug_heap(stdout, heap);
    struct block_header *block_header = get_block_header(block);

    // Проверка правильности
    if (!block || !header->is_free || !header->next || block_header->is_free || block_header->capacity.bytes < capacity * 2)
    {
        printf("\nFAILED!\n");
        return false;
    }

    _free(block);
    debug_heap(stdout, heap);

    free_heap(heap, size + REGION_MIN_SIZE);
    size_t heap_current_size = size_from_capacity(block_header->capacity).bytes;
    free_heap(block_header, heap_current_size);

    printf("\nPASSED!\n");
    return true;
}